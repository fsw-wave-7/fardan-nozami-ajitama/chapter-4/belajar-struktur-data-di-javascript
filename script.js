// var diskon = 500; // global scope
// // if (true) {
// //   var diskon = 300; // global scope
// // }

// // console.log(diskon);

// function diskonScope() {
//   var diskon = 300;
//   console.log(
//     `${diskon} ini nilai diskon yang ada di dalam function diskonScope `
//   );
// }
// diskonScope();
// console.log(`${diskon} ini nilai diskon yang ada di luar function`);

// function greeting() {
//   let message = "Halo boss";
//   console.log(message);
// }

// greeting();

// object literal
let ktp = {
  nama: "Fardan Nozami Ajitama",
  alamat: {
    jalan: "Jl. Rungkut Asri Timur XVII",
    kecamatan: "rungkut",
    Kota: "surabaya",
  },
  pekerjaan: ["drafter", "Web Developer"],
};
console.log(`Nama Saya ${ktp.nama}
alamat saya 
Jalan : ${ktp.alamat["jalan"]}
Kecamatan: ${ktp["alamat"]["kecamatan"]}
Kota: ${ktp.alamat.Kota}
`);

ktp.pekerjaan.forEach(function (e) {
  console.log(`${e} adalah pekerjaan ${ktp.nama}`);
});

const animals = ["chicken", "duck", "cow", "monkey"];
console.log(animals);
animals.push("elephant");
console.log("setelah di push menjadi");
console.log(animals);
animals.pop();
console.log("setelah di pop menjadi");
console.log(animals);
animals.shift();
console.log("setelah di shift menjadi");
console.log(animals);
animals.unshift("cat");
console.log("setelah di unshift menjadi");
console.log(animals);
animals.forEach(function (e, i) {
  console.log(`nama hewan yang ke ${i + 1} adalah ${e}`);
});

const numbers = [1, 3, 2, 5, 4, 3, 6, 7, 8, 9, 10, 11, 3, 2, 5, 7, 6, 12];
const filteredNumbers = numbers.filter((i) => {
  return i >= 5;
});
console.log(filteredNumbers);
